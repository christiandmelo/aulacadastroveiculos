<?php
require_once "../../../Conexao/Conexao.php";
require_once "../../Usuario.php";
require_once "../../BO/UsuarioBO.php";
require_once "../../DAO/UsuarioDAO.php";

session_start();

// Reccupera o usuario via sessao
$loginSession = $_SESSION["login"];

// Reccupera a conexao via sessao
$conexao = $_SESSION["conexao"];

// Cria um novo objeto do tipo usuário
$user = new Usuario();
$userBO = new UsuarioBO();
$userDAO = new UsuarioDAO();

if(!$_POST['novo']){
	// Retorno do servidor
	$result = $userBO->retornaUsuario($_POST['id'], $userDAO, $conexao);
	
	$consulta = $result->fetch(PDO::FETCH_OBJ);   
}

if (isset($consulta)){
		$nome = $consulta->nome;
		$ultimoNome = $consulta->ultimoNome;
		$cpf = $consulta->cpf;
		$rg = $consulta->rg;
		$dataNascimento = $consulta->dataNascimento;
		$idade = $consulta->idade;
		$endereco = $consulta->endereco;
		$login = $consulta->login;
		$senha = $consulta->senha;
		$email = $consulta->email;
	}
 ?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="myModalLabel">Novo Usuário</h4>
</div>
<div class="modal-body">
	<div id="RetornoMensagem"></div>
	<form role="form">
		<div class="form-group">
			<label>Nome:</label>
			<input id="nome" class="form-control" value=<?php if (isset($nome)) echo $nome;?>>
		</div>
		<div class="form-group">
			<label>Último Nome:</label>
			<input id="ultimoNome"class="form-control" value=<?php if (isset($ultimoNome)) echo $ultimoNome;?>>
		</div>
		<div class="form-group">
			<label>CPF:</label>
			<input id="rg" class="form-control" value=<?php if (isset($rg)) echo $rg;?>>
		</div>
		<div class="form-group">
			<label>RG:</label>
			<input id="cpf" class="form-control" value=<?php if (isset($cpf)) echo $cpf;?>>
		</div>
		<div class="form-group">
			<label>Data Nascimento:</label>
			<input id="dataNascimento" class="form-control" value=<?php if (isset($dataNascimento)) echo $dataNascimento;?>>
		</div>
		<div class="form-group">
			<label>Idade:</label>
			<input id="idade" class="form-control" value=<?php if (isset($idade)) echo $idade;?>>
		</div>
		<div class="form-group">
			<label>Endereço:</label>
			<input id="endereco" class="form-control" value=<?php if (isset($endereco)) echo $endereco;?>>
		</div>
		<div class="form-group">
			<label>Login:</label>
			<input id="login" class="form-control" value=<?php if (isset($login)) echo $login;?>>
		</div>
		<div class="form-group">
			<label>Senha:</label>
			<input id="senha" class="form-control" value=<?php if (isset($senha)) echo $senha;?>>
		</div>
		<div class="form-group">
			<label>Email:</label>
			<input id="email" class="form-control" value=<?php if (isset($email)) echo $email;?>>
		</div>
	</form>	 
	 <!--<input type=\"hidden\" name=\"typeControl\" value=\"create\">";-->
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php
	if($_POST['novo']){
		?>
		<button type="button" class="btn btn-primary" onclick="CadastrarUsuario()" >Salvar</button>
	<?php
	}else{
		?>
		<button type="button" class="btn btn-primary" onclick="EditarUsuario(<?=$_POST['id'];?>)" >Salvar</button>
	<?php
	}
	?>
</div>

<script type="text/javascript">
	function CadastrarUsuario(){
		AJajaxCarrega("Classes/Control/UsuarioControl.php",
					"typeControl=create&nome="+$("#nome").val()
					+"&ultimoNome="+$("#ultimoNome").val()
					+"&cpf="+$("#cpf").val()
					+"&rg="+$("#rg").val()
					+"&dataNascimento="+$("#dataNascimento").val()
					+"&idade="+$("#idade").val()
					+"&endereco="+$("#endereco").val()
					+"&login="+$("#login").val()
					+"&senha="+$("#senha").val()
					+"&email="+$("#email").val(),
					"RetornoMensagem"
					);
	}

	function EditarUsuario(Id){
		AJajaxCarrega("Classes/Control/UsuarioControl.php",
					"typeControl=update&id="+Id
					+"&nome="+$("#nome").val()
					+"&ultimoNome="+$("#ultimoNome").val()
					+"&cpf="+$("#cpf").val()
					+"&rg="+$("#rg").val()
					+"&dataNascimento="+$("#dataNascimento").val()
					+"&idade="+$("#idade").val()
					+"&endereco="+$("#endereco").val()
					+"&login="+$("#login").val()
					+"&senha="+$("#senha").val()
					+"&email="+$("#email").val(),
					"RetornoMensagem"
					);
	}
</script>