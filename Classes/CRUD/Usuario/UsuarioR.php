<?php 
require_once "../../../Conexao/Conexao.php";
require_once "../../Usuario.php";
require_once "../../BO/UsuarioBO.php";
require_once "../../DAO/UsuarioDAO.php";

session_start();

// Reccupera a conexao via sessao
$conexao = $_SESSION["conexao"];

// Cria um novo objeto do tipo usuário
$user = new Usuario();
$userBO = new UsuarioBO();
$userDAO = new UsuarioDAO();

// Retorno do servidor
$result = $userBO->retornaTodosUsuarios($userDAO, $conexao);
	
?>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Usuários</h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
	<button class="btn btn-primary btn-lg" onclick="NovoUsuario()" data-toggle="modal" data-target="#myModal">Novo Usuário</button>
		<div class="panel panel-default">
			<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Último Nome</th>
						<th>CPF</th>
						<th>Login</th>
						<th>Email</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while($consulta = $result->fetch(PDO::FETCH_OBJ)){
						?>
						<tr class="even gradeA">
							<td><?=$consulta->nome;?></td>
							<td><?=$consulta->ultimoNome;?></td>
							<td><?=$consulta->cpf;?></td>
							<td><?=$consulta->login;?></td>
							<td><?=$consulta->email;?></td>
							<td>
								<button class="btn btn-warning" onclick="EditaUsuario(<?=$consulta->id;?>)"  data-toggle="modal" data-target="#myModal">Editar</button>
								<button class="btn btn-danger" onclick="DeletaUsuario(<?=$consulta->id;?>)" >Excluir</button>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	function NovoUsuario(){
		AJajaxCarrega("Classes/CRUD/Usuario/UsuarioC.php",
					"novo=1",
					"ConteudoModal"
					);
	}

	function EditaUsuario(Id){
		AJajaxCarrega("Classes/CRUD/Usuario/UsuarioC.php",
					"novo=0&id="+Id,
					"ConteudoModal"
					);
	}

	function DeletaUsuario(Id){
		AJajaxCarrega("Classes/Control/UsuarioControl.php",
					"typeControl=delete&id="+Id,
					"retornoGeral"
		);
	}
</script>