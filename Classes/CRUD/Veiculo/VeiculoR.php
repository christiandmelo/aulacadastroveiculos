<?php 
require_once "../../../Conexao/Conexao.php";
require_once "../../Veiculo.php";
require_once "../../BO/VeiculoBO.php";
require_once "../../DAO/VeiculoDAO.php";

session_start();

// Reccupera a conexao via sessao
$conexao = $_SESSION["conexao"];

// Cria um novo objeto do tipo usuário
$veiculo = new Veiculo();
$veiculoBO = new VeiculoBO();
$veiculoDAO = new VeiculoDAO();

// Retorno do servidor
$result = $veiculoBO->retornaTodosVeiculos($veiculoDAO, $conexao);
	
?>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Veiculos</h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
	<button class="btn btn-primary btn-lg" onclick="NovoVeiculo()" data-toggle="modal" data-target="#myModal">Novo Veiculo</button>
		<div class="panel panel-default">
			<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
					<tr>
						<th>Foto</th>
						<th>Placa</th>
						<th>Grupo</th>
						<th>Nome</th>
						<th>Status</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while($consulta = $result->fetch(PDO::FETCH_OBJ)){
						?>
						<tr class="even gradeA">
							<td><?=$consulta->caminhofoto;?></td>
							<td><?=$consulta->placa;?></td>
							<td><?=$consulta->grupo;?></td>
							<td><?=utf8_encode($consulta->nome);?></td>
							<td><?=utf8_encode($consulta->status);?></td>
							<td>
								<button class="btn btn-warning" onclick="EditaVeiculo(<?=$consulta->id;?>)"  data-toggle="modal" data-target="#myModal">Editar</button>
								<button class="btn btn-danger" onclick="DeletaVeiculo(<?=$consulta->id;?>)" >Excluir</button>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	function NovoVeiculo(){
		AJajaxCarrega("Classes/CRUD/Veiculo/VeiculoC.php",
					"novo=1",
					"ConteudoModal"
					);
	}

	function EditaVeiculo(Id){
		AJajaxCarrega("Classes/CRUD/Veiculo/VeiculoC.php",
					"novo=0&id="+Id,
					"ConteudoModal"
					);
	}

	function DeletaVeiculo(Id){
		AJajaxCarrega("Classes/Control/VeiculoControl.php",
					"typeControl=delete&id="+Id,
					"retornoGeral"
		);
	}
</script>