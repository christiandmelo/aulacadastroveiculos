<?php
require_once "../../../Conexao/Conexao.php";
require_once "../../Veiculo.php";
require_once "../../BO/VeiculoBO.php";
require_once "../../DAO/VeiculoDAO.php";

session_start();

// Reccupera o Veiculo via sessao
$loginSession = $_SESSION["login"];

// Reccupera a conexao via sessao
$conexao = $_SESSION["conexao"];

// Cria um novo objeto do tipo usuário
$veiculo = new Veiculo();
$veiculoBO = new VeiculoBO();
$veiculoDAO = new VeiculoDAO();

if(!$_POST['novo']){
	// Retorno do servidor
	$result = $veiculoBO->retornaVeiculo($_POST['id'], $veiculoDAO, $conexao);
	
	$consulta = $result->fetch(PDO::FETCH_OBJ);   
}

if (isset($consulta)){
		$placa = $consulta->placa;
		$grupo = $consulta->grupo;
		$nome = $consulta->nome;
		$status = $consulta->status;
		$caminhofoto = $consulta->caminhofoto;
	}
 ?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="myModalLabel">Novo Veiculo</h4>
</div>
<div class="modal-body">
	<div id="RetornoMensagem"></div>
	<form role="form">
		<div class="form-group">
			<label>Placa:</label>
			<input id="placa" class="form-control" value=<?php if (isset($placa)) echo $placa;?>>
		</div>
		<div class="form-group">
			<label>Nome:</label>
			<input id="nome"class="form-control" value=<?php if (isset($placa)) echo $placa;?>>
		</div>
		<div class="form-group">
			<label>Grupo:</label>
			<select id="grupo" class="form-control">
				<option value="Carros" <?php if (isset($grupo)) if($grupo == "Carros") echo "Selected='selected'"; ?> >Carros</option>
				<option value="Motos" <?php if (isset($grupo)) if($grupo == "Motos") echo "Selected='selected'";?> >Motos</option>
				<option value="Caminhões" <?php if (isset($grupo)) if($grupo == "Caminhões") echo "Selected='selected'";?> >Caminhões</option>
				<option value="Aviões" <?php if (isset($grupo)) if($grupo == "Aviões") echo "Selected='selected'";?> >Aviões</option>
				<option value="Trens" <?php if (isset($grupo)) if($grupo == "Trens") echo "Selected='selected'";?> >Trens</option>
			</select>
		</div>
		<div class="form-group">
			<label>Status:</label>
			<select id="status" class="form-control">
				<option value="Disponível" <?php if (isset($status)) if($status == "Disponível") echo "Selected='selected'"; ?> >Disponível</option>
				<option value="Locado" <?php if (isset($status)) if($status == "Locado") echo "Selected='selected'"; ?> >Locado</option>
				<option value="Oficina" <?php if (isset($status)) if($status == "Oficina") echo "Selected='selected'"; ?> >Oficina</option>
			</select>
		</div>
	</form>	 
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php
	if($_POST['novo']){
		?>
		<button type="button" class="btn btn-primary" onclick="CadastrarVeiculo()" >Salvar</button>
	<?php
	}else{
		?>
		<button type="button" class="btn btn-primary" onclick="EditarVeiculo(<?=$_POST['id'];?>)" >Salvar</button>
	<?php
	}
	?>
</div>

<script type="text/javascript">
	function CadastrarVeiculo(){
		AJajaxCarrega("Classes/Control/VeiculoControl.php",
					"typeControl=create&placa="+$("#placa").val()
					+"&nome="+$("#nome").val()
					+"&grupo="+$("#grupo").val()
					+"&status="+$("#status").val()
					+"&caminhofoto=opa",
					"RetornoMensagem"
					);
	}

	function EditarVeiculo(Id){
		AJajaxCarrega("Classes/Control/VeiculoControl.php",
					"typeControl=update&id="+Id
					+"&placa="+$("#placa").val()
					+"&nome="+$("#nome").val()
					+"&grupo="+$("#grupo").val()
					+"&status="+$("#status").val()
					+"&caminhofoto=opa",
					"RetornoMensagem"
					);
	}
</script>