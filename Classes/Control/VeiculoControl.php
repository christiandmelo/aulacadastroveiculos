<?php 
	require_once "../../Conexao/Conexao.php";
	require_once "../Veiculo.php";
	require_once "../BO/VeiculoBO.php";
	require_once "../DAO/VeiculoDAO.php";
	
	session_start();

	// Reccupera o Veiculo via sessao
	$loginSession = $_SESSION["login"];
	
	// Reccupera a conexao via sessao
	$conexao = $_SESSION["conexao"];
	
	// recebe parametros do protocolo HTTP - Neste caso, via POST
	if (isset($_POST["id"])) $id = $_POST["id"];
	if (isset($_POST["placa"])) $placa = $_POST["placa"];
	if (isset($_POST["grupo"])) $grupo = $_POST["grupo"];
	if (isset($_POST["nome"])) $nome = $_POST["nome"];
	if (isset($_POST["status"])) $status = $_POST["status"];
	if (isset($_POST["caminhofoto"])) $caminhofoto = $_POST["caminhofoto"];
	if (isset($_POST["typeControl"])) $typeControl = $_POST["typeControl"];
	
	// Cria um novo objeto do tipo usuário
	$veiculo = new Veiculo();
	$veiculoBO = new VeiculoBO();
	$veiculoDAO = new VeiculoDAO();
	
	if ($typeControl == "create"){
		$veiculo->criarVeiculo($placa, $grupo, $nome, $status, $caminhofoto, $loginSession);
			
		$veiculoBO->inserirVeiculo($veiculo, $veiculoDAO, $conexao, $loginSession);
		?>
		<script type="text/javascript">
			alert("Veiculo criado com sucesso!!")
			CarregaMenu('Classes/CRUD/Veiculo/VeiculoR.php');
		</script>
		<?php
	}
	if ($typeControl == "update"){
		$veiculo->criarVeiculo($placa, $grupo, $nome, $status, $caminhofoto);
			
		$retorno = $veiculoBO->atualizarVeiculo($id, $veiculo, $veiculoDAO, $conexao);
		?>
		<script type="text/javascript">
			alert("Veiculo atualizado com sucesso!!")
			CarregaMenu('Classes/CRUD/Veiculo/VeiculoR.php');
		</script>
		<?php
	}
	if ($typeControl == "delete"){
		$veiculoBO->excluirVeiculo($id, $veiculoDAO, $conexao);
		?>
		<script type="text/javascript">
			alert("Veiculo excluído com sucesso!!")
			CarregaMenu('Classes/CRUD/Veiculo/VeiculoR.php');
		</script>
		<?php
	}
?>