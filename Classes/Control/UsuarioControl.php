<?php 
	require_once "../../Conexao/Conexao.php";
	require_once "../Usuario.php";
	require_once "../BO/UsuarioBO.php";
	require_once "../DAO/UsuarioDAO.php";
	
	session_start();

	// Reccupera o usuario via sessao
	$loginSession = $_SESSION["login"];
	
	// Reccupera a conexao via sessao
	$conexao = $_SESSION["conexao"];
	
	// recebe parametros do protocolo HTTP - Neste caso, via POST
	if (isset($_POST["id"])) $id = $_POST["id"];
	if (isset($_POST["nome"])) $nome = $_POST["nome"];
	if (isset($_POST["ultimoNome"])) $ultimoNome = $_POST["ultimoNome"];
	if (isset($_POST["cpf"])) $cpf = $_POST["cpf"];
	if (isset($_POST["rg"])) $rg = $_POST["rg"];
	if (isset($_POST["dataNascimento"])) $dataNascimento = $_POST["dataNascimento"];
	if (isset($_POST["idade"])) $idade = $_POST["idade"];
	if (isset($_POST["endereco"])) $endereco = $_POST["endereco"];
	if (isset($_POST["login"])) $login = $_POST["login"];
	if (isset($_POST["senha"])) $senha = $_POST["senha"];
	if (isset($_POST["email"])) $email = $_POST["email"];
	if (isset($_POST["typeControl"])) $typeControl = $_POST["typeControl"];
	
	// Cria um novo objeto do tipo usuário
	$user = new Usuario();
	$userBO = new UsuarioBO();
	$userDAO = new UsuarioDAO();
	
	if ($typeControl == "create"){
		$user->criarUsuario($nome, $ultimoNome, $cpf, $rg, 
			$dataNascimento, $idade, $endereco, $login, $senha, $email);
			
		$userBO->inserirUsuario($user, $userDAO, $conexao);
		?>
		<script type="text/javascript">
			alert("Usuário criado com sucesso!!")
			CarregaMenu('Classes/CRUD/Usuario/UsuarioR.php');
		</script>
		<?php
	}
	if ($typeControl == "update"){
		$user->criarUsuario($nome, $ultimoNome, $cpf, $rg, 
			$dataNascimento, $idade, $endereco, $login, $senha, $email);
			
		$retorno = $userBO->atualizarUsuario($id, $user, $userDAO, $conexao);
		?>
		<script type="text/javascript">
			alert("Usuário atualizado com sucesso!!")
			CarregaMenu('Classes/CRUD/Usuario/UsuarioR.php');
		</script>
		<?php
	}
	if ($typeControl == "delete"){
		$userBO->excluirUsuario($id, $userDAO, $conexao);
		?>
		<script type="text/javascript">
			alert("Usuário excluído com sucesso!!")
			CarregaMenu('Classes/CRUD/Usuario/UsuarioR.php');
		</script>
		<?php
	}
?>