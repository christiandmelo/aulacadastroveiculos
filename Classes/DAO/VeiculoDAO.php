<?php
     class VeiculoDAO
     {           
		public function insert($veiculo, $conexao, $login)
		{
			$con = $conexao->conectar();
			
			$query = "INSERT INTO veiculo(placa, grupo, nome, status, caminhofoto, user) 
						values(
							'" . $veiculo->getPlaca() . "',
							'" . $veiculo->getGrupo() . "',
							'" . utf8_decode($veiculo->getNome()) . "',
							'" . utf8_decode($veiculo->getStatus()) ."',
							'" . $veiculo->getCaminhofoto() . "',
							'" . $login . "'
						)";

			$stmt = $con->prepare($query);
			try{
				$stmt->execute();
			}catch(Excetion $e){
				var_dump($e);
			}
			return true;
		}
		
		public function update($id, $veiculo, $conexao)
		{
			$con = $conexao->conectar();
			
			$query = "UPDATE veiculo SET 
							placa='" . $veiculo->getPlaca() . "',
							grupo='" . $veiculo->getGrupo() . "',
							nome='" . utf8_decode($veiculo->getNome()) . "',
							status='" . utf8_decode($veiculo->getStatus()) . "',
							caminhofoto='" . $veiculo->getCaminhofoto() . "'
					  WHERE id = ".$id;
			
			$stmt = $con->prepare($query);
			return $stmt->execute();
		}
		
		public function delete($id, $conexao)
		{
			$con = $conexao->conectar();
			
			$query = "DELETE FROM veiculo WHERE id = ".$id;
			
			$stmt = $con->prepare($query);
			
			return $stmt->execute();
		}
		
		public function select($whereClause, $conexao)
		{
			$con = $conexao->conectar();
			
			if (isset($whereClause)) 
			{
				$query = "SELECT * FROM veiculo WHERE " . $whereClause;
			}
			else
			{
				$query = "SELECT * FROM veiculo";
			}
			
			$rs = $con->query($query);
			
			return $rs;
		}
     }
?>
