
<?php
	require_once "Pessoa.php";

     class Usuario extends Pessoa{
            private $login;
			private $senha;
			private $email;
            
            public function criarUsuario($nomeP, $ultimoNomeP, $cpfP, $rgP, 
				$dataNascimentoP, $idadeP, $enderecoP, $loginP, $senhaP, $emailP)
            {
				   $this->setNome($nomeP);
				   $this->setUltimoNome($ultimoNomeP);
				   $this->setCpf($cpfP);
				   $this->setRg($rgP);
				   $this->setDataNascimento($dataNascimentoP);
				   $this->setIdade($idadeP);
				   $this->setEndereco($enderecoP);
				   $this->setLogin($loginP);
				   $this->setSenha($senhaP);
				   $this->setEmail($emailP);
            }
			
			public function getLogin(){
				return $this->login;
			}
			
			public function setLogin($loginP){
				$this->login = $loginP;
			}
			
			public function getSenha(){
				return $this->senha;
			}
			
			public function setSenha($senhaP){
				$this->senha = $senhaP;
			}
			
			public function getEmail(){
				return $this->email;
			}
			
			public function setEmail($emailP){
				$this->email = $emailP;
			}
     }
?>
