
<?php

     class Veiculo{
            private $placa;
			private $grupo;
			private $nome;
			private $status;
			private $caminhofoto;
			private $user;
            
            public function criarVeiculo($placaP, $grupoP, $nomeP, $statusP, $caminhofotoP){
				   $this->setPlaca($placaP);
				   $this->setGrupo($grupoP);
				   $this->setNome($nomeP);
				   $this->setStatus($statusP);
				   $this->setCaminhofoto($caminhofotoP);
            }
			
			public function getPlaca(){
				return $this->placa;
			}
			
			public function setPlaca($placaP){
				$this->placa = $placaP;
			}
			
			public function getGrupo(){
				return $this->grupo;
			}
			
			public function setGrupo($grupoP){
				$this->grupo = $grupoP;
			}
			
			public function getNome(){
				return $this->nome;
			}
			
			public function setNome($nomeP){
				$this->nome = $nomeP;
			}

			public function getStatus(){
				return $this->status;
			}
			
			public function setStatus($statusP){
				$this->status = $statusP;
			}

			public function getCaminhofoto(){
				return $this->caminhofoto;
			}
			
			public function setCaminhofoto($caminhofotoP){
				$this->caminhofoto = $caminhofotoP;
			}

			public function getUser(){
				return $this->user;
			}
			
			public function setUser($userP){
				$this->user = $userP;
			}
     }
?>
