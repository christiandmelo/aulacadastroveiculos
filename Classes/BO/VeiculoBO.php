
<?php
     class VeiculoBO{  
		public function inserirVeiculo($veiculo, $veiculoDAO, $conexao, $login){
			return $veiculoDAO->insert($veiculo, $conexao, $login);
		}
		
		public function atualizarVeiculo($id, $veiculo, $veiculoDAO, $conexao){
			return $veiculoDAO->update($id, $veiculo, $conexao);
		}
		
		public function excluirVeiculo($loginp, $veiculoDAO, $conexao){
			return $veiculoDAO->delete($loginp, $conexao);
		}
		
		public function retornaVeiculo($id, $veiculoDAO, $conexao){		
			return $veiculoDAO->select("id = '$id'", $conexao);
		}
		
		public function retornaTodosVeiculos($veiculoDAO, $conexao){		
			return $veiculoDAO->select(null, $conexao);
		}
     }
?>
