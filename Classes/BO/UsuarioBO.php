
<?php
     class UsuarioBO{  
		public function inserirUsuario($user, $userDAO, $conexao){
			return $userDAO->insert($user, $conexao);
		}
		
		public function atualizarUsuario($id, $user, $userDAO, $conexao){
			return $userDAO->update($id, $user, $conexao);
		}
		
		public function excluirUsuario($loginp, $userDAO, $conexao){
			return $userDAO->delete($loginp, $conexao);
		}
	 
		public function verificaUsuario($loginp, $senhap, $userDAO, $conexao){		
			return $userDAO->select("login = '$loginp' AND senha = '$senhap'", $conexao);
		}
		
		public function retornaUsuario($id, $userDAO, $conexao){		
			return $userDAO->select("id = '$id'", $conexao);
		}
		
		public function retornaTodosUsuarios($userDAO, $conexao){		
			return $userDAO->select(null, $conexao);
		}
     }
?>
