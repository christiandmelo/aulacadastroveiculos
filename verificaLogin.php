<?php
	session_start();

	require_once "Conexao/Conexao.php";
	require_once "Classes/Usuario.php";
	require_once "Classes/BO/UsuarioBO.php";
	require_once "Classes/DAO/UsuarioDAO.php";	
	
	// recebe parametros do protocolo HTTP - Neste caso, via POST
	$login = $_POST["login"];
	$senha = $_POST["senha"];
	
	// Estabelece conexão com o banco de dados
	$conexao = new Conexao();
	
	// Cria um novo objeto do tipo usuário
	$user = new Usuario();
	$userBO = new UsuarioBO();
	$userDAO = new UsuarioDAO();
	
	// Verifica se o usuário recebido via parâmetro existe no banco de dados
	$executeDB = $userBO->verificaUsuario($login, $senha, $userDAO, $conexao);
	
	// Após verificação valida se encontrou o usuário
	if ($executeDB->rowCount() > 0){		
		// Armazena usuario e conexao em sessão!
		$_SESSION["login"] = $login;
        $_SESSION["conexao"] = $conexao;
        $_SESSION["logado"] = true;
		
		// Armazena o login no cookie!
		setcookie("loginCookie", $login, time()+3600 * 24 * 30);  // Dura um mes!
		
		?>
        <script type="text/javascript">
            window.location = "index.php";
        </script>
        <?php
	} else {
        ?>
        <script type="text/javascript">
            alert("Usuário ou senha incorretos!");
            window.location = "index.php";
        </script>
        <?php
	}
?>